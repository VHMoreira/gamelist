
export default class Game{

    Game(id, nome, descricao, nota, url, jogado) {
        this.id = id;
        this.nome = nome;
        this.descricao = descricao;
        this.nota = nota;
        this.url = url;
        this.jogado = jogado;
    }
    
}