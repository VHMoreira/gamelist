import axios from 'axios';

async function initState({ commit }) {
    commit('SET_STATE');
}

async function insertGame({ commit }, game) {
    commit('INSERT_STATE', game);
}

async function deleteGame({ commit }, id) {
    commit('DELETE_STATE', id);
}

async function updateGame({ commit }, game) {
    commit('SET_JOGADO', game);
}

export {
    initState,
    insertGame,
    deleteGame,
    updateGame
}
