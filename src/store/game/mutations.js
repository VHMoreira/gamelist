import Vue from 'vue';
import axios from 'axios';

const baseURL = 'http://localhost:8080/games'

async function SET_STATE(state) {
    const {data} = await axios.get("http://localhost:8080/games");
    data.forEach(e => {
        Vue.set(state.games, e.id, e);
    });
}

async function SET_JOGADO(state, payload) {
    payload.jogado = !payload.jogado
    await axios.put(`http://localhost:8080/games/${payload.id}`, payload);
    Object.assign(state.games[payload.id], payload.updates);
}

async function INSERT_STATE(state, payload) {
    const { data } = await axios.post('http://localhost:8080/games', payload);
    Vue.set(state.games, data.id, data);
}

async function DELETE_STATE(state, id) {
    console.log(id);
    await axios.delete(`http://localhost:8080/games/${id}`);
    Vue.delete(state.games, id);
}

export {
    SET_STATE,
    SET_JOGADO,
    DELETE_STATE,
    INSERT_STATE
}
